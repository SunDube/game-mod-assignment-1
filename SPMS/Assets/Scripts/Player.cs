﻿/**
 *  @Player.cs
 *  @version: 1.00
 *  @author: Jesse Freeman
 *  @date: Feb 3
 *  @copyright (c) 2012 Jesse Freeman, under The MIT License (see LICENSE)
 * 
 * 	-- For use with Weekend Code Project: Unity's New 2D Workflow book --
 *
 *  This script will allow you to move a GameObject, idealy the player,
 *  via the keyboard and the mouse. This will slide GameObject forward
 *  based on the direction of the input.
 */

using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
    public float speed = 200;
    public float maxSpeed = 5;
    int moving = 0;
    float mouseX = 0;

    Rigidbody2D rigidbody2DComponent;

    void Awake() {
        rigidbody2DComponent = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update() {


        if (Input.GetKey("right") || Input.GetKey("d")) {
            moving = 1;
        } else if (Input.GetKey("left") || Input.GetKey("a")) {
            moving = -1;
        } else {
            moving = 0;
        }

        if (moving != 0) {
            float velocityX = System.Math.Abs(rigidbody2DComponent.velocity.x);

            if (velocityX < .5) {
                rigidbody2DComponent.AddForce(new Vector2(moving, 0) * speed);

                if (this.transform.localScale.x != moving)
                    this.transform.localScale = new Vector3(moving, 1, 1);
            }

            if (velocityX > maxSpeed)
                rigidbody2DComponent.velocity = new Vector2(maxSpeed * moving, 0);
        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Healing : MonoBehaviour
{
    public GameObject playerCharacter;
    public int healingAmount;
    public float coolDown;
    private float nextUse = 0;


    public void Heal() {

        if (Time.time > nextUse)
        {
            //checks if maximum healing has been reached
            if (playerCharacter.GetComponent<Health>().health < playerCharacter.GetComponent<Health>().maxHealth)
            {
                //increases player health by 1
                playerCharacter.GetComponent<Health>().health += healingAmount;
            }

            nextUse = Time.time + coolDown;

        }
        else
        {
            Debug.Log("Ability is On Cooldown");
        }

    }

    public string AbilityStatusH() {
        if (Time.time < nextUse)
        {
            return "On Cooldown";
        }
        else {
            return "Ready";
        }

    }
    
}

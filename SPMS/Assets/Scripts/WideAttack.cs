﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WideAttack : MonoBehaviour
{
    public GameObject playerCharacter;
    private GameObject[] targets;
    public string attackTags;
    public float landDelay;
    public float coolDown;
    private float nextUse = 0;


    public void StartMultiAttack()
    {
        if (Time.time > nextUse)
        {
            //empty the array as to not target object that nolonger exist
            targets = null;
            //fill the array with the appropriate objects
            targets = GameObject.FindGameObjectsWithTag(attackTags);
            //preform multiattack
            PrepareAttack();

            nextUse = Time.time + coolDown;
        }
        else
        {
            Debug.Log("Ability is On Cooldown");
        }
    }

    private void PrepareAttack() {

        //make the player character jump for visual feedback
        playerCharacter.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 3), ForceMode2D.Impulse);

        //boost the attack value for instakill
        playerCharacter.GetComponent<Attack>().attackValue = 2;

        //wait for monster to hit the ground before inflicting damage
        Invoke("LaunchAttack",landDelay);

        //reduce the attack value for back to normal
        playerCharacter.GetComponent<Attack>().attackValue = 1;



    }

    
    //wait for the monster to hit the ground before inflicting damage

    private void LaunchAttack() {
     
        //attack all the objects in the array
        for (int i = 0; i < targets.Length; i++)
        {
            playerCharacter.GetComponent<Attack>().AttackTarget(targets[i]);
        }

    }

    //return the current cooldown state
    public string AbilityStatusW()
    {
        if (Time.time < nextUse)
        {
            return "On Cooldown";
        }
        else
        {
            return "Ready";
        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Defend : MonoBehaviour
{
    public GameObject playerCharacter;
    public float defenseTime;
    public float coolDown;
    private float nextUse = 0;
    private bool currentlyDefending;

    //changes boolean in health script and block the object from taking damage
    public void DefendObject() {
        if (Time.time > nextUse)
        {
            playerCharacter.GetComponent<Health>().defending = true;
            currentlyDefending = true;
            Invoke("LowerDefenses", defenseTime);

            nextUse = Time.time + coolDown;
        }
        else {
            Debug.Log("Ability is On Cooldown");
        }
    }

    //makes the object take damage again
    void LowerDefenses() {
        playerCharacter.GetComponent<Health>().defending = false;
        currentlyDefending = false;
    }

    //return the current cooldown state
    public string AbilityStatusD()
    {
        if (Time.time < nextUse)
        {
            return "On Cooldown";
        }
        else
        {
            return "Ready";
        }

    }

    //return the current cooldown state
    public string BlockStatus()
    {
        if (currentlyDefending == true)
        {
            return "Blocking Damage";
        }
        else
        {
            return "Not Blocking Damage";
        }

    }


}

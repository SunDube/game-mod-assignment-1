﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateStatus : MonoBehaviour
{
    public GameObject abilities;
    public string abilityName;
    private string status;

    void Start()
    {
        
    }

    void Update()
    {
        SetAbility();
        gameObject.GetComponent<Text>().text = status;
    }

    //find which ability is being monitered
    void SetAbility() {
        if (abilityName == "Jump Attack")
        {
            status = abilities.GetComponent<WideAttack>().AbilityStatusW();
        }

        if (abilityName == "Healing")
        {
            status = abilities.GetComponent<Healing>().AbilityStatusH();
        }

        if (abilityName == "Defend")
        {
            status = abilities.GetComponent<Defend>().AbilityStatusD();
        }

        if (abilityName == "Blocking")
        {
            status = abilities.GetComponent<Defend>().BlockStatus();
        }


    }
}
